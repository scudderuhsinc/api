import { ServerResponse, IncomingMessage } from 'http';

export type RequestCookies = { [key: string]: string };
export type RequestQuery = { [key: string]: string | string[] };
export type RequestBody = any;

export type Request = IncomingMessage & {
    query: RequestQuery;
    cookies: RequestCookies;
    body: RequestBody;
};

export type Response = ServerResponse & {
    send: (body: any) => Response;
    json: (jsonBody: any) => Response;
    status: (statusCode: number) => Response;
    redirect: (statusOrUrl: string | number, url?: string) => Response;
};