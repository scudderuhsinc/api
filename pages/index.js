

export default function Home() {
  return (
    <div>
      <head>

        <title>Universal Directory, Application Programming Interface (API) | UHS, Inc.</title>
        <link rel="icon" href="/uhs-icon.png" />
        
      </head>

      <main>

        <header>
          <a href="https://www.uhsinc.com" target="_blank">
            <img alt="Universal Health Services, Inc." src="https://cdn.sanity.io/images/89obqmi5/directory/86a65e8f6589013ac0143acb91d0171a682dced6-101x45.svg?w=2000&h=2000&fit=max" />
          </a>
        </header>

        <h1>Welcome</h1>

        <p>This is the welcome message and brief description of this Application Programming Interface (API) for the <a href="https://directory.uhsinc.com" target="_blank">Universal Directory</a></p>

        <h2>Supported Document Types</h2>

        <h3>Locations</h3>

        <ul>
          <li><a href="https://blog.logrocket.com/using-authentication-in-next-js/">Auth tutorial</a></li>
          <li><a href="/api/locations/">All Locations</a></li>
          <li><a href="/api/location/sid/edd9826f-64cd-44d1-b6c0-13ffcd69eaa8/">Location by Sanity ID (SID)</a></li>
          <li><a href="/api/location/slug/manatee-memorial-hospital/">Location by SLUG</a></li>
        </ul>

        <h3>Providers</h3>

        <ul>
          <li><em>Coming Soon</em></li>
        </ul>

      </main>

      <footer>
        
        <p>footer</p>
      
      </footer>

    </div>
  )
}
