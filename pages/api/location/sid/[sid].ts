import { Request, Response } from '../../../../lib/types'
import sanityClient from '@sanity/client'

function sanityMiddleware(req: Request, res: Response, sid: any ) {

    const client = sanityClient({
        projectId: process.env.SANITY_PROJECT_ID,
        dataset: process.env.SANITY_DATASET,
        // apiVersion: '2021-03-25',
        token: '', // or leave blank for unauthenticated usage
        useCdn: true, // `true` or `false` if you want to ensure fresh data
    })

    const query: string = `*[_type == "location" && location._id == "`+ sid +`" ] {
        'Location Identifier': _id,
        'Profile Definition Name':'roofTop',
        'AutoSync':'yes',
        'Public Location Name': name,
        'Long Description': descLong[0].children[0].text,
        'Short Description': descShort,
        'Name': name,
        'Categories': select(
            services[0].serviceLine->name match "Emergency" => "Emergency Rooms",
            services[0].diagnosis->name match "Substance Use Disorders" => "Addiction Treatment Center",
            services[0].serviceLine->name match "Psychiatry" => "Mental Health Clinic",
            "Hospital"
        ),
        'Office 1 Office Name': name,
        'Office 1 Address Line 1': addr.line1,
        'Office 1 Address Line 2': addr.line2,
        'Office 1 City': addr.city,
        'Office 1 State': addr.state,
        'Office 1 Postal Code': addr.code,
        'Office 1 Country': select(
            addr._type == 'usAddr' => 'us',
            addr._type == 'ukAddr' => 'uk',
        ),
        'Office 1 Longitude':geopoint.lng,
        'Office 1 Latitude':geopoint.lat,
        'Office 1 Phone 1 Type': 'Local',
        'Office 1 Phone Number': telNum.main,
        'Office 1 Phone 1 Country': select(
            addr._type == 'usAddr' => 'us',
            addr._type == 'ukAddr' => 'uk',
        ),
        'Office 1 Email Addresses': 'default@value.needed',
        'Office 1 Website 1 Type': 'Homepage',
        'Office 1 Website 1 Url': link.home,
        'Office 1 Business Hours 1 Name': 'Main',
        'Office 1 Business Hours 1 Sunday Hours': select(
            hrs_247 => '00:00-00:00',
            hrs.sundayOpen.hour >= '00' => hrs.sundayOpen.hour+":"+hrs.sundayOpen.minute+"-"+hrs.sundayClose.hour+":"+hrs.sundayClose.minute,
            hrs.sundayOpen.hour != '00' => 'x',
        ),
        'Office 1 Business Hours 1 Monday Hours': select(
            hrs_247 => '00:00-00:00',
            hrs.mondayOpen.hour >= '00' => hrs.mondayOpen.hour+":"+hrs.mondayOpen.minute+"-"+hrs.mondayClose.hour+":"+hrs.mondayClose.minute,
            hrs.mondayOpen.hour != '00' => 'x',
        ),
        'Office 1 Business Hours 1 Tuesday Hours': select(
            hrs_247 => '00:00-00:00',
            hrs.tuesdayOpen.hour >= '00' => hrs.tuesdayOpen.hour+":"+hrs.tuesdayOpen.minute+"-"+hrs.tuesdayClose.hour+":"+hrs.tuesdayClose.minute,
            hrs.tuesdayOpen.hour != '00' => 'x',
        ),
        'Office 1 Business Hours 1 Wednesday Hours': select(
            hrs_247 => '00:00-00:00',
            hrs.wednesdayOpen.hour >= '00' => hrs.wednesdayOpen.hour+":"+hrs.wednesdayOpen.minute+"-"+hrs.wednesdayClose.hour+":"+hrs.wednesdayClose.minute,
            hrs.wednesdayOpen.hour != '00' => 'x',
        ),
        'Office 1 Business Hours 1 Thursday Hours': select(
            hrs_247 => '00:00-00:00',
            hrs.thursdayOpen.hour >= '00' => hrs.thursdayOpen.hour+":"+hrs.thursdayOpen.minute+"-"+hrs.thursdayClose.hour+":"+hrs.thursdayClose.minute,
            hrs.thursdayOpen.hour != '00' => 'x',
        ),
        'Office 1 Business Hours 1 Friday Hours': select(
            hrs_247 => '00:00-00:00',
            hrs.fridayOpen.hour >= '00' => hrs.fridayOpen.hour+":"+hrs.fridayOpen.minute+"-"+hrs.fridayClose.hour+":"+hrs.fridayClose.minute,
            hrs.fridayOpen.hour != '00' => 'x',
        ),
        'Office 1 Business Hours 1 Saturday Hours': select(
            hrs_247 => '00:00-00:00',
            hrs.saturdayOpen.hour >= '00' => hrs.saturdayOpen.hour+":"+hrs.saturdayOpen.minute+"-"+hrs.saturdayClose.hour+":"+hrs.saturdayClose.minute,
            hrs.saturdayOpen.hour != '00' => 'x',
        ),
        'Media Cover Url': select(
            gallery[] != null => gallery[0].asset->url,
        ),
        'Media Logo Url': select(
            logo_horizontal != null => logo_horizontal.asset->url,
        ),
    }`
    
    return new Promise((resolve, reject) => {
        client.fetch(query).then( (result) => {
            if (result instanceof Error) {
                // console.log("reject: "+result.toString())
                return reject(result)
            }
            //console.log(`result: `+JSON.stringify(result))
            return resolve(result)
        })
    })
}

async function handler(req: Request, res: Response ) {
    const {
        query: { sid, name },
        method,
    } = req

    switch (method) {
        case 'GET':
            res.status(200).json( await sanityMiddleware(req, res, sid ) )
            // console.log(`response: `+JSON.stringify(res))
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }
}

export default handler