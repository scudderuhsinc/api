import { Request, Response } from '../../../../lib/types'
import sanityClient from '@sanity/client'

function sanityMiddleware(req: Request, res: Response, slug: any ) {

    const client = sanityClient({
        projectId: process.env.SANITY_PROJECT_ID,
        dataset: process.env.SANITY_DATASET,
        //apiVersion: '2021-03-25',
        token: '', // or leave blank for unauthenticated usage
        useCdn: false, // `true` or `false` if you want to ensure fresh data
    })

    console.log(slug)

    const query: string = '*[_type == "location" && slug.current == "'+ slug +'" ] {_id, name, slug }'
    console.log(query)

    return new Promise((resolve, reject) => {
        client.fetch(query).then( (result) => {
            if (result instanceof Error) {
                //console.log("reject: "+result.toString())
                return reject(result)
            }
            return resolve(result)
        })
    })
}

async function handler(req: Request, res: Response ) {
    const {
        query: { slug, name },
        method,
    } = req

    switch (method) {
        case 'GET':
            res.status(200).json( await sanityMiddleware(req, res, slug ) )
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }
}

export default handler