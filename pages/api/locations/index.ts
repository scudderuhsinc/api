import { Request, Response } from '../../../lib/types'
import sanityClient from '@sanity/client'

// https://nextjs.org/docs/api-routes/api-middlewares
// https://www.sanity.io/docs/js-client

function sanityMiddleware(req: Request, res: Response) {

    const client = sanityClient({
        projectId: process.env.SANITY_PROJECT_ID,
        dataset: process.env.SANITY_DATASET,
        //apiVersion: '2021-03-25',
        token: '', // or leave blank for unauthenticated usage
        useCdn: false, // `true` or `false` if you want to ensure fresh data
    })

    const query: string = '*[_type == "location"] {_id, name, slug, vertical}'
    
    return new Promise((resolve, reject) => {
        client.fetch(query).then( (result) => {
            if (result instanceof Error) {
                //console.log("reject: "+result.toString())
                return reject(result)
            }
            //console.log("resolve: "+result.toString())
            return resolve(result)
        })
    })
}

async function handler(req: Request, res: Response ) {
    const {
        query: { name },
        method,
        headers,
    } = req

    // console.log(headers)
    switch (method) {
        case 'GET':
            res.status(200).json( await sanityMiddleware(req, res) )
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }
}

export default handler