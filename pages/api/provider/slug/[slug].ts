export default function handler(req, res) {
    const {
        query: { slug, name },
        method,
    } = req

    console.log(
        "project id: "+process.env.SANITY_PROJECT_ID+"\n"+
        "project dataset: "+process.env.SANITY_DATASET
    )

    switch (method) {
        case 'GET':
            // Get data from your database
            res.status(200).json({ slug, name: `Location SLUG: ${slug}` })
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }
}