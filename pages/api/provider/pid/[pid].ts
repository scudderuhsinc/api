import { Request, Response } from '../../../../lib/types'

export default function handler(req: Request, res: Response ) {
    const {
        query: { pid, name },
        method,
    } = req

    console.log(
        "project id:      "+process.env.SANITY_PROJECT_ID+"\n"+
        "project dataset: "+process.env.SANITY_DATASET
    )

    switch (method) {
        case 'GET':
            // Get data from your database
            res.status(200).json({ pid, name: `Provider ID: ${pid}` })
            break
        default:
            res.setHeader('Allow', ['GET'])
            res.status(405).end(`Method ${method} Not Allowed`)
    }
}