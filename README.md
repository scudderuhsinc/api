# Universal Directory Application Programming Interface

The Universal Directory, **UniDir**, is an internally managed database powering internal and external service integrations. The **UniDir API** is a public resource on the internet following the conventions of a RESTful Application Programming Interface, _API_. The **UniDir API** allows for external and internal clients, **API Consumers**, to make _HTTP requests_ providing up to date information for serivice integrations. The data, returned in the form of a _HTTP response_, can be used by a website's Content Managent System, within a _webhook_ or a 3rd party Data Management System.

## API Consumers

If an API Consumer is making requests from outside the protection of Universal Health Services's secure network, their _HTTP requests_ need provide credentials for their _request_ to be processed correctly. This ensures the data returned to them is formated to meet the requirements of their platform.
If a _HTTP request_ is submitted without credentials or is not registered as a user, the _HTTP response_ will be processed as a GUEST user. _Requests_ by GUESTs have limited permissions and the _responses_ are not preformatted. The data is a raw dump of the information and structures, defined within the Universal Directory.

The **UniDir API** uses customized headers to ensure a consumer can access the data they need and in the format that they can process. To register your service integration, submit this form. When your application is approved you will recieve your _HTTP request_ key:value pair.

Additional Resources:

- _HTTP headers_ let the client and the server pass additional information with an HTTP request or response .. [MDN Web Docs](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers)
- 'Headers' allow you to set custom HTTP headers for an incoming request path .. [Next.js Docs](https://nextjs.org/docs/api-reference/next.config.js/headers)

### Reputation.com

$ curl -H "From:api@reputation.com" https://api.mydomain.com/api/locations

http://localhost:3000/api/location/sid/-here- 0044fdfa-1566-4756-9d85-1263b8d08eaa

http://localhost:3000/api/location/slug/-here- arbour-counseling-services-woburn

A description of the required data types and format for the service integration.

- How the data as it is managed within the Universal Directory

- **TODO** items required to support the integration

- The formated data as it would appear in the _UniDir API_ Response for consumption by Reputation.com. As defined by Reputation.com [documentation](https://api.reputation.com/v3/docs).

#### Location

`here is an example of a API request for a Location by Reputation.com`

##### Action
___

Requested action for a Location record managed within the Reputation.com platform, i.e.:

- add, `a`
- change, `c`
- delete, `d`

###### Universal Directory

_na_

Request actions are not managed within the Universal Directory.

**TODO** : defined by a Location's "draft", "published"/"unpublished" or "archived" state.

###### Reputation:

"add/change/delete"

- `"add/change/delete":"a"` defines an _add_ request for a location.
- `"add/change/delete":"c"` defines a _change_ request for a location.
- `"add/change/delete":"d"` defines a _delete_ request for a location.

##### Unique Identifier
___

A unique identifier required for each location record.

###### Universal Directory

a Location document's "id"

- `location._id`

###### Reputation

"Location Identifier"

- `"Location Identifier":"a-locations-id-sring-here"`

##### Profile Type
___

###### Universal Directory

A "location" document type.

- `location`

###### Reputation

"Profile Definition Name"

`RoofTop` for a physical location such as a hospital

- **always** defined as `"Profile Definition Name":"RoofTop"`

##### Sync
___

The requested synchronization setting for a Location record managed within the Reputation.com platform.

###### Universal Directory

_na_

Synchronization actions are not managed within the Universal Directory.

**TODO** : defined by a Location's "draft", "published"/"unpublished" or "archived" state.

###### Reputation:

"AutoSync"

`Yes` Reputation.com should publish this information on 3rd party sites.

`No` Reputation.com should NOT publish information anywhere.

- `"AutoSync":"yes"`

##### Status
___

The requested synchronization setting for a Location record managed within the Reputation.com platform.

###### Universal Directory

_na_

Location status are not managed within the Universal Directory.

**TODO** : defined by a Location's "draft", "published"/"unpublished" or "archived" state.

###### Reputation:

"Office 1 Status State"

- `"Office 1 Status State":"state-here"`

##### Name
___

The location name you want displayed on public sites managed by Reputation.com

###### Universal Directory

Name

- `location.name`

###### Reputation:

"Public Location Name"

- `"Public Location Name":"Location Name Here"`

##### Business Category
____

Category for Google My Business page [documentation](https://support.google.com/business/)

###### Universal Directory

_na_

Google My Business Category Names are not managed within the Universal Directory.

Category Names are defined by a Location's Treatment Services, each consisting of a _Service Line_ and an optional _Diagnosis_ determing a single category. From the first (primary) record in the Treatment Services array:

```
'Categories': select(
```

- IF offers the “Emergency” _Service Line_ = Emergency Rooms

```
    services[0].serviceLine->name match "Emergency" => "Emergency Rooms",
```

- IF offers the “Psychiatric” _Service Line_ AND has a _Diagnosis_ of “Substance Use Disorder” = Addiction Treatment Center

```
    services[0].diagnosis->name match "Substance Use Disorders" => "Addiction Treatment Center",
```

- IF offers the “Psychiatric” _Service Line_ = Mental Health Clinic

```
    services[0].serviceLine->name match "Psychiatry" => "Mental Health Clinic",
```

- ELSE = Hospital

```
    "Hospital"
),
```

##### Reputation

"Categories"

- `"Categories":"Google My Business Category Name Here"`


##### Long Description
___

###### Universal Directory

Long Description

- `descLong[0].children[0].text`

###### Reputation

"Long Description"

- `"Long Description":"Our hospital was found in 1956 by xxxx xxxxx, we pride ourselves on our ability to deliver xxxx with best in class customer service and satisfaction."`

##### Office Name
___

Same as a "Name"

###### Universal Directory

"Name"

- `location.name`

###### Reputation

"Office 1 Office Name", same as a "Public Location Name"

- `"Office 1 Office Name":"Location Name Here"`

##### Address, Line 1
___

e.g. 123 Main St.

###### Universal Directory

"Line 1" for either a US or UK address

- `location.usAddr.line1`
- `location.ukAddr.line1`

###### Reputation

"Office 1 Address Line 1"

- `"Office 1 Address Line 1":"123 Main St."`

##### Address, Line 2
___

e.g. Suite 101

###### Universal Directory

"Line 2" for a US address

- `location.usAddr.line2`

"Line 2" + "Locality" for a UK address

- `location.ukAddr.line2`
- `location.ukAddr.locality`

**TODO** : define logic to format a UK addresses containing the optional "Locality" field.

###### Reputation

"Office 1 Address Line 2"

- `"Office 1 Address Line 1":"Suite 101"`

##### Address, City
___

e.g. Scottsdale

###### Universal Directory

"City" for either a US or UK address

- `location.usAddr.city`
- `location.ukAddr.city`

###### Reputation

"Office 1 City"

- defined as `"Office 1 City":"Scottsdale"`

##### Address, State
___

e.g. AZ

###### Universal Directory

"State" for either a US or UK address

- `location.usAddr.state`
- `location.ukAddr.state`

###### Reputation

"Office 1 State"

- `"Office 1 State":"AZ"`

##### Address, Postal Code
___

e.g. 85257

###### Universal Directory

"Code" for either a US or UK address

- `location.usAddr.code`
- `location.ukAddr.code`

###### Reputation

"Office 1 Postal Code"

- `"Office 1 Postal Code":"85257"`

##### Address, Country
___

i.e. The United States of America (US) or The United Kingdom (UK)

###### Universal Directory

_na_

A location's country is not directly defined within the Universal Directory. Either an associated "usAddr" or "ukAddr" record implies a location's parent country.

- `location.usAddr`
- `location.ukAddr`

**TODO** : define logic to explicitly define a location's parent country.

###### Reputation

"Office 1 Country"

- `"Office 1 Country":"us"`
- `"Office 1 Country":"uk"`

##### Longitude
___

###### Universal Directory

- `geopoint.lng`

###### Reputation

"Office 1 Longitude"

- `"Office 1 Longitude":"here"`

##### Latitude
___

###### Universal Directory

- `geopoint.lat`

###### Reputation

"Office 1 Latitude"

- `"Office 1 Latitude":"here"`

##### Phone Type
___

###### Universal Directory

i.e. either "main" or "fax" number.

- `location.telNum.main`
- `location.telNum.fax`

###### Reputation

"Office 1 Phone 1 Type"

- **always** defined as `"Office 1 Phone 1 Type":"Local"`

##### Phone Number
___

e.g. 480-555-4825

###### Universal Directory

"main"

- `location.telNum.main`

###### Reputation

"Office 1 Phone Number"

- "Office 1 Phone Number":"480-555-4825"

##### Phone Country
___

i.e. The United States of America (US) or The United Kingdom (UK)

###### Universal Directory

_na_

A location's country is not directly defined within the Universal Directory. Either an associated "usAddr" or "ukAddr" record implies a location's parent country.

- `location.usAddr`
- `location.ukAddr`

**TODO** : confirm logic to explicitly define a location's parent country.

###### Reputation

"Office 1 Phone 1 Country"

- `"Office 1 Phone 1 Country":"us"`
- `"Office 1 Phone 1 Country":"uk"`

##### Email
___

e.g. info@location.com

###### Universal Directory

_na_

A location's email is not directly defined within the Universal Directory.

**TODO** : define logic to define a location's default email address based from website.

###### Reputation

"Office 1 Email Addresses"

- "Office 1 Email Addresses":"info@location.domain"

##### Website Type
___

###### Universal Directory

"home"

- `location.link.home`

###### Reputation

"Office 1 Website 1 Type"

- **always** defined as `"Office 1 Website 1 Type":"Homepage"`

##### Website
___

A location's website, URL e.g. https://www.location.domain/here

###### Universal Directory

"home"

- `location.link.home`

###### Reputation

"Office 1 Website 1 Url"

- `"Office 1 Website 1 Url":"https://www.location.domain/here"`

#### Hours Name
___

###### Universal Directory

_na_

A location's business hours are not named within the Universal Directory.

###### Reputation

"Office 1 Business Hours 1 Name"

- **always** defined as `"Office 1 Business Hours 1 Name":"Main"`

#### Hours Type
___

###### Universal Directory

_na_

A location's business hours are not given a type within the Universal Directory.

###### Reputation

"Office 1 Business Hours 1 Type"

- **always** defined as `"Office 1 Business Hours 1 Type":"Main"`

##### 24/7 Hours
___

If a location is open 24 hours a day, 7 days a week

###### Universal Directory

- `location.hrs_247` = true

###### Reputation

For each day of the week.

- "Office 1 Business Hours 1 `each-day` Hours":"00:00-00:00"

```
{
    "Office 1 Business Hours 1 Sunday Hours":"00:00-00:00", 
    "Office 1 Business Hours 1 Monday Hours":"00:00-00:00",
    "Office 1 Business Hours 1 Tuesday Hours":"00:00-00:00",
    "Office 1 Business Hours 1 Wednesday Hours":"00:00-00:00",
    "Office 1 Business Hours 1 Thursday Hours":"00:00-00:00",
    "Office 1 Business Hours 1 Friday Hours":"00:00-00:00",
    "Office 1 Business Hours 1 Saturday Hours":"00:00-00:00"
}
```
##### Hours

If a location is not open 24/7, opening and closing hours for each day of the week in military format or defined as "closed" using a 'x'

###### Universal Directory

NOT open 24/7

- `location.hrs_247` = false or _undefined_

Monday hours would be defined as 

- `hrs.mondayOpen.hour >= '00' =>`
- `hrs.mondayOpen.hour+":"+hrs.mondayOpen.minute` = '08:30' i.e. 8:30am
- `+"-"+`
- `hrs.mondayClose.hour+":"+hrs.mondayClose.minute` = '17:00' i.e. 5:00pm
- returning "08:30-05:00"

and when a location returns both opening and closing times of  _undefined_, that location is "closed" for that day, i.e. closed on Saturdays

- `hrs.saturdayOpen.hour != '00' => "x"` = _undefined_

###### Reputation

- "Office 1 Business Hours 1 `each-day` Hours": "##:##-##:##"

Monday hours would be defined as 

```
{
    Sunday,
    "Office 1 Business Hours 1 Monday Hours":"09:00-17:00",
    Tuesday, ..
}
```

and closed on Saturdays defined with an 'x' 

- `"Office 1 Business Hours 1 Saturday Hours":"x"`

##### Short Description
___

e.g. This is a short description.

###### Universal Directory

"Short Description"

- `location.descShort`

###### Reputation

"Short Description"

Required only for Facebook

- "Short Description":"This is a short description."

#### Location Image
___

###### Universal Directory

"Image Gallery"

The first image in the locations image gallery.

- `location.gallery[0].src`

###### Reputation

"Media Cover Url"

- "Media Cover URL":"https://cdn.url.com/location-exterior-image-name.jpg"

#### Location Logo
___

###### Universal Directory

"Logo"

- `location.horizontal-logo.src`

###### Reputation

"Media Logo Url"

- "Media Logo Url":"https://cdn.url.com/location-logo-image-name.jpg"

___
___

### Provider

`here is an example of a API request for a Provider by Reputation.com`


##### Action
___

Requested action for a Provider record managed within the Reputation.com platform, i.e.:

- add, `a`
- change, `c`
- delete, `d`

###### Universal Directory

_na_

Request actions are not managed within the Universal Directory.

**TODO** : defined by a Provider's "draft", "published"/"unpublished" or "archived" state.

###### Reputation:

"add/change/delete"

- `"add/change/delete":"a"` defines an _add_ request for a provider.
- `"add/change/delete":"c"` defines a _change_ request for a provider.
- `"add/change/delete":"d"` defines a _delete_ request for a provider.


##### Profile Type
___

###### Universal Directory

A "provider" document type.

- `provider`

###### Reputation

"Profile Definition Name"

`Physician` for a healthcare Provider

- **always** defined as `"Profile Definition Name":"Physician"`


##### Sync
___

The requested synchronization setting for a Provider record managed within the Reputation.com platform.

###### Universal Directory

_na_

Synchronization actions are not managed within the Universal Directory.

**TODO** : defined by a Provier's "draft", "published"/"unpublished" or "archived" state.

###### Reputation:

"AutoSync"

`Yes` Reputation.com should publish this information on 3rd party sites.

`No` Reputation.com should NOT publish information anywhere.

- `"AutoSync":"yes"`


##### Primary Location
___

The main location name where this Provider practices.

###### Universal Directory

"Primary Location Name"

- `provider.primary.location.name`

**TODO** : define logic

###### Reputation

"Public Location Name"

- `"Public Location Name":"Location Name Here"`

"Office 1 Office Name"

- `"Office 1 Office Name":"Location Name Here"`


##### Categories
___

Category for Google My Business page [documentation](https://support.google.com/business/)

###### Universal Directory
**TODO** : defined by the Treatment Services offered by a Provider's Primary Location, each consisting of a _Service Line_ and an optional _Diagnosis_ determing a single category.

###### Reputation


##### Long Description
___

i.e. "Our brand was found in 1956 by xxxx xxxxx, we pride ourselves on our ability to deliver xxxx with best in class customer service and satisfaction."

###### Universal Directory

###### Reputation


##### Medical NPI
___

A Provider's National Provider Identifier (NPI) [standard](https://www.cms.gov/Regulations-and-Guidance/Administrative-Simplification/NationalProvIdentStand) | [find a Provider's NPI](https://npiregistry.cms.hhs.gov/)

###### Universal Directory

###### Reputation


##### Professional Title
___

###### Universal Directory

###### Reputation


##### Appellation
___

###### Universal Directory

###### Reputation


##### First Name
___

###### Universal Directory

###### Reputation


##### Middle Name
___

###### Universal Directory

###### Reputation


##### Last Name
___

###### Universal Directory

###### Reputation


#### Gender
___

###### Universal Directory

###### Reputation


#### Birthday
___

###### Universal Directory

###### Reputation


#### Biography
___

###### Universal Directory

###### Reputation


#### Medical Employer Affiliation
___

###### Universal Directory

###### Reputation


#### State License Number
___

###### Universal Directory

###### Reputation


#### Accepting New Patients
___

###### Universal Directory

###### Reputation


#### Insurances Accepted
___

###### Universal Directory

###### Reputation


#### Degrees
___

###### Universal Directory

###### Reputation


#### Accepted Types of Payment
___

###### Universal Directory

###### Reputation


#### Short Description
___

Required only for Facebook

###### Universal Directory

###### Reputation